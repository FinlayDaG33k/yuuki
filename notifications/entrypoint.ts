import { env, Logger, Webserver } from "./deps.ts";
import { Notifications } from "./src/notifications.ts";
import { Bootstrap } from "../lib/communication/bootstrap.ts";

// Load our env file
const bootstrap = await Bootstrap.getEnv('notifications');
if(!bootstrap) Deno.exit(1);

// Load routes
import "./src/routes.ts";

// Load env
env({
  path: './.env',
  export: true
});

// Load notifications
Logger.info(`Loading notifications...`);
await Notifications.load();
Logger.info(`Notifications loaded!`);

// Start websocket server
Logger.info(`Starting webserver on port "${Number(Deno.env.get("WEBSERVER_PORT")) ?? 80}"...`);
const server = new Webserver(Number(Deno.env.get("WEBSERVER_PORT")) ?? 80);
server.start();
Logger.info(`Webserver server started!`);
