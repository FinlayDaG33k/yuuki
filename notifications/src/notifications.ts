import { Logger } from "../deps.ts";
import { INotification } from "./interfaces/INotification.ts";
import { MasterEvent } from "../../lib/communication.ts";

export class Notifications {
  private static currentId: number = 0;
  public static errorLevel = 0;
  private static items: INotification[] = [];

  public static add(notification: any = {}): INotification {
    // Get the current time
    let date = new Date();

    // Make sure all variables are set
    if(!notification.service) notification.service = 'Unknown';
    if(!notification.severity) notification.severity = 'Unknown';
    if(!notification.message) notification.message = 'Missing message';

    // Add our date
    notification.time = {
      raw: date,
      formatted: Notifications.getTime(date)
    }

    // Set our ID
    notification.id = Notifications.currentId++;

    // Add notification to the items
    Notifications.items.push(notification);

    // Finish up
    Notifications.sortItems();
    Notifications.getErrorLevel();
    Notifications.store();

    // Try to dispatch an event
    MasterEvent.dispatch('NOTIFICATION_UPDATE', Notifications.get());

    // Return the current notification
    return notification;
  }

  public static find(id: number|null = null, service: string, severity: string = 'warning', message: string = '') {
    // Check if we specified an ID
    // If so, find an item by this id
    if(typeof id === 'number') {
      return Notifications.items.find((item: INotification) => {
        return item.id === id;
      });
    }

    // No ID was specified, find by contents
    return Notifications.items.find((item: INotification) => {
      return item.service === service && item.severity === severity && item.message === message;
    });
  }

  public static get(limit: number = -1) {
    if(limit === -1) return Notifications.items;
    return Notifications.items.slice(-limit);
  }

  public static remove(id: number) {
    // Check if an id was set
    if(typeof id === 'undefined' || typeof id === null) return;

    // Get the index of the notification
    let index = Notifications.items.findIndex((item) => { return item.id === id; });

    // Check if the notification was found
    if(index === -1) return;

    // Remove the notification
    Notifications.items.splice(index, 1);

    // Store our notifications
    Notifications.store();

    // Try to dispatch an event
    MasterEvent.dispatch('NOTIFICATION_UPDATE', Notifications.get());
  }

  public static clear() {
    // Clear notifications
    Notifications.items = [];

    // Store notifications
    Notifications.store()

    // Try to dispatch an event
    MasterEvent.dispatch('NOTIFICATION_UPDATE', Notifications.get());
  }

  public static getErrorLevel() {
    // Check if we have errors
    if(Notifications.items.filter(notification => notification.severity === 'error').length > 0) {
      Notifications.errorLevel = 2;
      return;
    }

    // Check if we have warnings
    if(Notifications.items.filter(notification => notification.severity === 'warning').length > 0) {
      Notifications.errorLevel = 1;
      return;
    }

    // Nothing appears to be wrong
    Notifications.errorLevel = 0;
  }

  private static getTime(date: Date) {
    let year = date.getFullYear(),
      month = date.getMonth(),
      day = date.getDate(),
      min = date.getMinutes(),
      sec = date.getSeconds(),
      hour = date.getHours();

    return ('0' + day).slice(-2) + '/' +
      ('0' + month).slice(-2) + '/' +
      year +
      ' ' +
      (hour < 10 ? ("0" + hour) : hour) + ":" +
      (min < 10 ? ("0" + min) : min) + ":" +
      (sec < 10 ? ("0" + sec) : sec);
  }

  private static sortItems() {
    Notifications.items.sort(function(a: INotification, b: INotification) {
      if(typeof b.time === 'undefined') return -1;
      if(typeof a.time === 'undefined') return 1;
      return new Date(b.time.raw).getTime() - new Date(a.time.raw).getTime();
    });
  }

  private static store() {
    Deno.writeTextFile(`${Deno.env.get("DATA_DIR")}/notifications.json`, JSON.stringify(Notifications.items));
  }

  public static async load() {
    try {
      await Deno.stat(`${Deno.env.get("DATA_DIR")}/notifications.json`);
    } catch(e) {
      Logger.warning(`No existing notifications were found`);
      Logger.debug(e.message);
      return;
    }

    Logger.info(`Found existing notifications!`);
    try {
      // Parse JSON and set the data
      Notifications.items = JSON.parse(await Deno.readTextFile(`${Deno.env.get("DATA_DIR")}/notifications.json`));

      // Filter out "id-less" notifications
      Notifications.items = Notifications.items.filter((item: INotification) => item.id !== null);

      // Check if we have notifications
      // If not, return here
      if(Notifications.items.length <= 0) return;

      // Update our next id
      let lastId = Math.max(...Notifications.items.map((notification: INotification) => {
        if(typeof notification.id === 'undefined') return 0;
        return notification.id;
      }));
      if(lastId !== null) Notifications.currentId = lastId + 1;
    } catch(e) {
      Logger.error(`Could not load JSON from notifications!`);
      Logger.debug(e);
    }
  }
}
