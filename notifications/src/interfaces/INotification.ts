export interface INotification {
  id: number;
  service: string;
  severity?: string;
  message?: string;
  time?: {
    raw: any;
    formatted: string;
  }
}
