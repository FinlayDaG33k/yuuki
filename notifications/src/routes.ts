import { Router } from "../deps.ts";

Router.add({ path: '/list', controller: 'Notifications', action: 'index', method: 'GET' });
Router.add({ path: '/add', controller: 'Notifications', action: 'add', method: 'PUT' });
Router.add({ path: '/remove/:id', controller: 'Notifications', action: 'remove', method: 'DELETE' });
Router.add({ path: '/remove-all', controller: 'Notifications', action: 'removeAll', method: 'DELETE' });
