import { Controller, RouteArgs } from "../../deps.ts";
import { INotification } from "../interfaces/INotification.ts";
import { Notifications } from "../notifications.ts";

export class NotificationsController extends Controller {
  public async index(args: RouteArgs) {
    super.type = 'application/json';
    super.set('data', Notifications.get());
  }

  public async add(args: RouteArgs) {
    // Make sure a body was found
    if(args.body === '') {
      super.set('message', 'ERR_EMPTY_BODY');
      super.type = 'text/plain';
      return;
    }

    // Decode the JSON body
    const data: INotification = JSON.parse(args.body);

    // Add our notification
    try {
      Notifications.add(data);
    } catch(e) {
      super.status = 500;
      super.set('message', 'INTERNAL_SERVER_ERROR');
      super.type = 'text/plain';
      return;
    }

    super.set('message', 'OK');
    super.type = 'text/plain';
  }

  public async remove(args: RouteArgs) {
    // Make sure an id is set
    if(!args.params.hasOwnProperty('id')) {
      super.set('message', 'ERR_EMPTY_ID');
      super.type = 'text/plain';
      return;
    }

    // Add our notification
    try {
      Notifications.remove(Number(args.params.id));
    } catch(e) {
      super.status = 500;
      super.set('message', 'INTERNAL_SERVER_ERROR');
      super.type = 'text/plain';
      return;
    }

    super.set('message', 'OK');
    super.type = 'text/plain';
  }

  public async removeAll(arg: RouteArgs) {
    try {
      Notifications.clear();
    } catch(e) {
      super.status = 500;
      super.set('message', 'INTERNAL_SERVER_ERROR');
      super.type = 'text/plain';
      return;
    }
  }
}
