import { Time } from "../common/time.ts";

export interface IServerStatus {
  server: string;
  status: string;
  time: Time;
}
