import { WebSocketServer, WebSocketAcceptedClient } from "https://deno.land/x/websocket@v0.1.3/mod.ts";
import { Logger } from "../logging.ts";
import { Database, Notifications } from "../communication.ts";
import { Events } from "./events.ts";
import { Authorizer } from "../auth/authorizer.ts";

export class Websocket {
  private readonly port: number = 80
  private readonly authorize: boolean = false;
  private server: WebSocketServer|null = null;

  constructor(port: number = 80, authorize: boolean = false) {
    this.port = port;
    this.authorize = authorize;
  }

  public start() {
    this.server = new WebSocketServer(this.port, Deno.env.get('REAL_IP_HEADER') ?? null);
    this.server.on("connection", (client: WebSocketAcceptedClient, url: string) => {
      Logger.info(`New WebSocket connection from "${(client.webSocket.conn.remoteAddr as Deno.NetAddr).hostname!}"...`);

      // Authorize
      if(this.authorize === true && !Authorizer.client(url.replace('/', ''))) {
        Logger.warning(`Closing connection with "${(client.webSocket.conn.remoteAddr as Deno.NetAddr).hostname!}": Invalid token!`);
        client.close(1000, 'Invalid authentication token!');
        return;
      }

      // Send initial data
      this.pushConnect(client);

      // Add handlers
      client.on("message", (message: string) => this.onMessage(message));
    });
  }

  public async broadcast(eventString: string, data: any) {
    // Make sure the server has started
    if(!this.server) return;

    // Loop over each client
    // Check whether they are still alive
    // Send the event to the clients that are still alive
    for(let client of this.server.clients) {
      if(!client) continue;
      if(client.isClosed) continue;
      client.send(JSON.stringify({
        event: eventString,
        data: data
      }));
    }
  }

  private async onMessage(message: string) {
    // Check if a message was set
    if(!message) return;

    // Decode the message
    let data = JSON.parse(message);
    // Get the Event
    let event = data.event;
    let tokens = [];
    for(let token of event.split('_')) {
      token = token.toLowerCase();
      token = token[0].toUpperCase() + token.slice(1);
      tokens.push(token);
    }
    event = tokens.join('');

    const handler = Events.getHandler(event);
    if(!handler) return Logger.warning(`Event "${event}" does not exists! (did you register it?)`);

    // Import the controller file
    const imported = await import(`file://${Deno.cwd()}/src/events/${handler.handler}.event.ts`);

    // Instantiate the controller
    const controller = new imported[`${event}Event`]();

    // Execute the execute method
    try {
      await controller['execute'](data.data);
    } catch(e) {
      Logger.error(e.message);
    }
  }

  /**
   * Push data when a new client connects
   */
  public async pushConnect(client: WebSocketAcceptedClient) {
    // Make sure the client is still alive
    if(!client) return;
    if(client.isClosed) return;

    // Push notifications
    client.send(JSON.stringify({
      event: 'NOTIFICATION_UPDATE',
      data: await Notifications.list()
    }));

    // Push Cryptoticker
    client.send(JSON.stringify({
      event: 'DATABASE_UPDATE',
      data: {
        key: 'crypto-tickers',
        content: await Database.get('crypto-tickers')
      }
    }));

    // Push serverstatus
    client.send(JSON.stringify({
      event: 'DATABASE_UPDATE',
      data: {
        key: 'server-statuses',
        content: await Database.get('server-statuses')
      }
    }));

    // Push UPS status
    client.send(JSON.stringify({
      event: 'DATABASE_UPDATE',
      data: {
        key: 'ups-state',
        content: await Database.get('ups-state')
      }
    }));

    // Push Schedule
    client.send(JSON.stringify({
      event: 'DATABASE_UPDATE',
      data: {
        key: 'ical-schedule',
        content: await Database.get('ical-schedule')
      }
    }));

    // Push Weather
    client.send(JSON.stringify({
      event: 'DATABASE_UPDATE',
      data: {
        key: 'weather',
        content: await Database.get('weather')
      }
    }));
  }
}
