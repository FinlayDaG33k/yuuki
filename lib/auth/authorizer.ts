export class Authorizer {
  public static client(token: string = '') {
    if(!token) return false;
    return token === Deno.env.get('CLIENT_AUTH');
  }
}
