import { Logger } from "../logging/logger.ts";
import { INotification } from "../interfaces/notification.interface.ts";

export class Notifications {
  public static async list() {
    let resp;
    try {
      resp = await fetch(`${Deno.env.get('NOTIFICATION_HOST') ?? ''}/list`, {
        method: 'GET',
      });

      // Get the content from the response
      return await resp.json();
    } catch(e) {
      Logger.error(`Could not add notification`);
      Logger.debug(e.message);
      return;
    }
  }

  public static async add(notification: INotification) {
    let resp;
    try {
      resp = await fetch(`${Deno.env.get('NOTIFICATION_HOST') ?? ''}/add`, {
        method: 'PUT',
        body: JSON.stringify(notification)
      });

      // Get the content from the response
      const content = await resp.text();
      if(content !== 'OK') throw Error(content);
    } catch(e) {
      Logger.error(`Could not add notification`);
      Logger.debug(e.message);
      return;
    }
  }

  public static async remove(id: number) {
    let resp;
    try {
      resp = await fetch(`${Deno.env.get('NOTIFICATION_HOST') ?? ''}/remove/${id}`, {
        method: 'DELETE'
      });

      // Get the content from the response
      const content = await resp.text();
      if(content !== 'OK') throw Error(content);
    } catch(e) {
      Logger.error(`Could not remove notification`);
      Logger.debug(e.message);
      return;
    }
  }

  public static async removeAll() {
    let resp;
    try {
      resp = await fetch(`${Deno.env.get('NOTIFICATION_HOST') ?? ''}/remove-all`, {
        method: 'DELETE'
      });

      // Get the content from the response
      const content = await resp.text();
      if(content !== 'OK') throw Error(content);
    } catch(e) {
      Logger.error(`Could not remove all notifications`);
      Logger.debug(e.message);
      return;
    }
  }
}
