import { Logger } from "../logging.ts";

export class Bootstrap {
  public static async getEnv(module: string): Promise<boolean> {
    let resp;
    try {
      resp = await fetch(`${Deno.env.get('MASTER_HOST') ?? ''}/bootstrap/${module}`, {
        method: 'GET',
        headers: {
          authorization: `Bearer ${Deno.env.get('BOOTSTRAP_TOKEN') ?? ''}`
        }
      });

      // Make sure we have a 200 status
      if(resp.status !== 200) throw Error(await resp.text());

      // Get the content from the response
      const data = await resp.text();

      // Write to env file
      await Deno.writeTextFile(`${Deno.cwd()}/.env`, data);

      return true;
    } catch (e) {
      Logger.error(`Could not get env`);
      Logger.error(e.message);
      return false;
    }
  }
}
