import { Logger } from "../logging/logger.ts";

export class Database {
  public static async get(key: string): Promise<string|null> {
    let resp;
    try {
      resp = await fetch(`${Deno.env.get('DATABASE_HOST') ?? ''}/get/${key}`, {
        method: 'GET',
      });

      // Get the content from the response
      const content = await resp.text();

      // Decode the JSON
      const data = JSON.parse(content);

      // Return our content
      return data.result;
    } catch(e) {
      Logger.error(`Could not get key "${key}"`);
      Logger.debug(e.message);
      return null;
    }
  }

  public static async set(key: string, value: any) {
    let resp;
    try {
      resp = await fetch(`${Deno.env.get('DATABASE_HOST') ?? ''}/set/${key}`, {
        method: 'PUT',
        body: value
      });

      // Get the content from the response
      const content = await resp.text();
      if(content !== 'OK') throw Error(content);
    } catch(e) {
      Logger.error(`Could not set key "${key}"`);
      Logger.debug(e.message);
      return;
    }
  }
}
