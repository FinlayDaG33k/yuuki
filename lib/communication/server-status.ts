import { Logger } from "../logging/logger.ts";
import { IServerStatus } from "../interfaces/serverstatus.interface.ts";

export class ServerStatuses {
  public static async update(service: string, status: string) {
    let resp;
    try {
      resp = await fetch(`${Deno.env.get('STATUS_HOST') ?? ''}/update/${service}`, {
        method: 'PATCH',
        body: status
      });

      // Get the content from the response
      const content = await resp.text();
      if(content !== 'OK') throw Error(content);
    } catch(e) {
      Logger.error(`Could not update server status`);
      Logger.debug(e.message);
      return;
    }
  }
}
