import { Logger } from "../logging/logger.ts";

export class MasterEvent {
  public static async dispatch(event: string, data: any) {
    let resp;
    try {
      resp = await fetch(`${Deno.env.get('MASTER_HOST') ?? ''}/event`, {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${Deno.env.get('AUTH_SECRET') ?? ''}`
        },
        body: JSON.stringify({
          event: event,
          data: data
        })
      });

      // Get the content from the response
      const content = await resp.text();
      if(content !== 'OK') throw Error(content);
    } catch(e) {
      Logger.error(`Could not dispatch event to master`);
      Logger.debug(e.message);
      return;
    }
  }
}
