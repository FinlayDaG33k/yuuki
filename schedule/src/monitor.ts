import { Logger, cron, Time } from "../deps.ts";
import { Database } from "../../lib/communication/database.ts";

export class Monitor {
  private readonly icsUrl: string = '';
  private icsContent: any = {};
  private appointments: any[] = [];
  private isUpdating: boolean = false;

  public constructor() {
    Logger.info(`Starting monitor for "Schedule"...`);

    // Set the URL to our ics
    this.icsUrl = `${Deno.env.get('ICAL_HOST')}/${Deno.env.get('ICAL_PATH')}`;
    cron('* * * * *', async () => { await this.update(); });
  }

  private async update() {
    // Make sure we're not already updating
    if(this.isUpdating === true) return;

    // Mark as updating
    this.isUpdating = true;
    Logger.debug(`Updating Schedule...`);

    // Make sure our internet is up
    const hasInternet = await Database.get('internet-status');
    if(!hasInternet) {
      this.isUpdating = false;
      return Logger.warning(`No internet, skipping update...`);
    }

    // Download our ICS file
    const ics = await this.download();

    // Parse our ICS file
    this.icsContent = await this.parse(ics);

    // Get relevant appointments
    await this.week();

    // Push to database
    await Database.set('ical-schedule', JSON.stringify(this.appointments));

    // Mark as finished
    this.isUpdating = false;
  }

  private async week() {
    // Get all events for this week
    const weekEventsKeys = Object.keys(this.icsContent).filter(key => {
      let entry = this.icsContent[key].date;
      let today = new Time().addDay(1);
      let week = new Time().addWeek(1).midnight();

      // Filter everything
      // - Before today
      // - After next week
      if(entry.getTime < today.getTime) return false;
      if(entry.getTime > week.getTime) return false;

      // Return leftovers
      return true;
    }).reverse();

    // Make sure we have entries
    if(weekEventsKeys.length === 0) {
      this.appointments = [];
      return;
    }

    // Add all appointments
    this.appointments = [];
    for(let eventKey of weekEventsKeys) {
      const event = this.icsContent[eventKey];
      this.appointments.push(event);
    }

    // Sort appointments
    this.appointments.sort((a: any, b: any) => {
      if(a.date.getTime < b.date.getTime) return -1;
      if(a.date.getTime > b.date.getTime) return 1;
      return 0;
    });
  }

  private async download() {
    return await fetch(this.icsUrl)
      .then(response => response.text());
  }

  private async parse(ics: string) {
    // Split at each new line
    let lines = ics.split("\r\n");

    // Hold our events
    let events: any = {};
    let events_i = 0;

    // Loop over each line in our ICS
    // Add them to our events
    for (let i = 0; i < lines.length; i++) {
      if (lines[i].includes('DTSTART')) {
        var date = lines[i].split(":");
        events[events_i] = {date: this.getDate(date[1])};
        continue;
      }

      if (lines[i].includes('SUMMARY')) {
        var title = lines[i].split(":");
        events[events_i]["title"] = title[1];
        continue;
      }

      if (lines[i].includes('END:VEVENT')) {
        events_i++;
        continue;
      }
    }

    return events;
  }

  private getDate(date: string): Time {
    // Build our regex
    const regex = /([0-9]{4})([0-9]{2})([0-9]{2})T?([0-9]{2})?([0-9]{2})?([0-9]{2})?([A-Z])?/;

    // Execute our regex
    const matches = regex.exec(date);

    // Exit if no matches found
    if(!matches) throw new Error('Could not parse date!');

    // Building the date in a proper format
    let datestring = '';
    if(matches[1]) datestring += matches[1];
    if(matches[2]) datestring += '-';
    if(matches[2]) datestring += matches[2];
    if(matches[3]) datestring += '-';
    if(matches[3]) datestring += matches[3];
    if(matches[4]) datestring += 'T';
    if(matches[4]) datestring += matches[4];
    if(matches[5]) datestring += ':';
    if(matches[5]) datestring += matches[5];
    if(matches[6]) datestring += ':';
    if(matches[6]) datestring += matches[6];
    if(matches[7]) datestring += matches[7];

    // Return a new Time instance
    return new Time(datestring);
  }
}
