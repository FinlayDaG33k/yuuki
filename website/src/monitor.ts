import { Logger, cron } from "../deps.ts";
import { ServerStatuses } from "../../lib/communication/server-status.ts";
import { Database } from "../../lib/communication.ts";

interface IStatus {
  domain: string;
  port: number;
  status_code: number;
  response_ip: string;
  response_code: number;
  response_time: number;
}

export class Monitor {
  private api: string = 'https://isitup.org';
  private readonly host: string = 'www.finlaydag33k.nl';
  private isUpdating: boolean = false;

  public constructor() {
    Logger.info(`Starting monitor for "Website"...`);

    // Set our hostname from the env
    if(typeof Deno.env.get('WEBSITE_HOST') !== 'undefined') {
      Logger.info(`Changing hostname to "${Deno.env.get('WEBSITE_HOST')!}"`);
      this.host = Deno.env.get('WEBSITE_HOST')!;
    } else {
      Logger.warning(`Using default server "${this.host}"`);
    }

    // Run updater
    this.update();
    cron('* * * * *', async () => { await this.update(); });
  }

  private async update() {
    // Make sure we're not already updating
    if(this.isUpdating === true) return;

    // Mark as updating
    this.isUpdating = true;
    Logger.debug(`Updating Website status...`);

    // Make sure the internet is up
    const hasInternet = await Database.get('internet-status');
    if(!hasInternet) {
      await ServerStatuses.update('Website', 'unknown');
      this.isUpdating = false;
      return Logger.warning(`No internet, skipping test...`);
    }

    // Update our status
    const status = await this.test();
    await ServerStatuses.update('Website', status === true ? 'up' : 'down');
    Logger.debug(`Website test completed! (status: ${status === true ? 'up' : 'down'})`);

    // Mark as finished
    this.isUpdating = false;
  }

  private async test(): Promise<boolean> {
    let resp: IStatus;
    try {
      resp = await fetch(`${this.api}/${this.host}.json`, {
        method: 'GET'
      }).then(resp => resp.json());
    } catch(e) {
      Logger.error(`Could not test website status: ${e.message}`);
      return false;
    }

    return resp.response_code === 200;
  }
}
