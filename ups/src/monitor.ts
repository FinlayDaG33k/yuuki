import { Logger, cron } from "../deps.ts";
import { Nut } from "./nut.ts";
import { Database } from "../../lib/communication/database.ts";

interface IUpsState {
  maxWatt: number;
  loadPercent: number;
  loadWatt: number;
  chargePercent: number;
  remainingSeconds: number;
}

export class Monitor {
  private nut: Nut = new Nut(<string>Deno.env.get('NUT_HOST'));
  private state: IUpsState = {
    maxWatt: 0,
    loadPercent: 0,
    loadWatt: 0,
    chargePercent: 0,
    remainingSeconds: 0
  }

  public constructor() {
    Logger.info(`Starting monitor for "UPS"...`);

    // Start the monitor
    this.start();
  }

  private async start() {
    Logger.info(`Connecting to NUT server...`);
    await this.nut.connect();

    Logger.info(`Obtaining max load for UPS "${Deno.env.get('UPS_NAME')}"...`);
    this.state.maxWatt = await this.nut.getPowerLimit(Deno.env.get('UPS_NAME'));
    Logger.debug(`Max load for UPS "${Deno.env.get('UPS_NAME')}" reported as "${this.state.maxWatt}W"`);

    // Start the cronjob updater
    await this.update();
    cron('*/10 * * * * *', async () => {
      await this.update();
    });
  }

  private async update() {
    // Get the current load percent
    Logger.debug(`Obtaining current load for UPS "${Deno.env.get('UPS_NAME')}"...`);
    this.state.loadPercent = await this.nut.getLoad(Deno.env.get('UPS_NAME'));
    Logger.debug(`Current load for UPS "${Deno.env.get('UPS_NAME')}" reported as "${this.state.loadPercent}%"`);

    // Calculate the load in Watts
    this.state.loadWatt = (this.state.loadPercent > 0) ? Number((( this.state.loadPercent *  this.state.maxWatt) / 100).toFixed(2)) : 0;
    Logger.debug(`Current load for UPS "${Deno.env.get('UPS_NAME')}" reported as "${this.state.loadWatt}W"`);

    // Get the current battery charge
    Logger.debug(`Obtaining current charge for UPS "${Deno.env.get('UPS_NAME')}"...`);
    this.state.chargePercent = await this.nut.getCharge(Deno.env.get('UPS_NAME'));
    Logger.debug(`Current charge for UPS "${Deno.env.get('UPS_NAME')}" reported as "${this.state.chargePercent}%"`);

    // Get the estimated remaining runtime
    Logger.debug(`Obtaining estimated runtime left for UPS "${Deno.env.get('UPS_NAME')}"...`);
    this.state.remainingSeconds = await this.nut.getRuntime(Deno.env.get('UPS_NAME'));
    Logger.debug(`Estimated runtime left for UPS "${Deno.env.get('UPS_NAME')}" reported as "${this.state.remainingSeconds}s"`);

    // Update database
    await Database.set(`ups-state`, JSON.stringify(this.state));
  }
}
