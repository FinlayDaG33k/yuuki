import { Logger, cron } from "../deps.ts";
import { Database, Notifications, ServerStatuses } from "../../lib/communication.ts";

interface ITradePair {
  crypto: string;
  fiat: string;
  value: number;
  change: number;
}

interface ITicker {
  bid: number;
  bid_size: number;
  ask: number;
  ask_size: number;
  daily_change: number;
  daily_change_relative: number;
  last_price: number,
  volume: number;
  high: number,
  low: number
}

export class Monitor {
  private api: string = 'https://api-pub.bitfinex.com/v2/ticker';
  private tickers: ITradePair[] = [];
  private isUpdating: boolean = false;

  public constructor() {
    Logger.info(`Starting monitor for "Crypto"...`);

    // Add our initial tradepairs
    this.tickers.push({crypto: 'BTC', fiat: 'USD', value: 0.00, change: 0.00});
    this.tickers.push({crypto: 'BTC', fiat: 'EUR', value: 0.00, change: 0.00});
    this.tickers.push({crypto: 'ETH', fiat: 'USD', value: 0.00, change: 0.00});
    this.tickers.push({crypto: 'ETH', fiat: 'EUR', value: 0.00, change: 0.00});

    // Update tickers now
    this.update();

    // Add cron job to update tickers
    cron(`* * * * *`, async () => { await this.update(); });
  }

  public async update(): Promise<void> {
    // Make sure we're not already updating
    if(this.isUpdating === true) return Logger.debug(`Crypto update already running!`);

    // Mark as updating
    this.isUpdating = true;

    // Make sure the internet is up
    const hasInternet = await Database.get('internet-status');
    if(!hasInternet) {
      await ServerStatuses.update('BitFinex', 'unknown');
      this.isUpdating = false;
      return Logger.warning(`No internet, skipping test...`);
    }

    // Update crypto tickers
    Logger.debug(`Attempting to get cryptoticker...`);
    for (let pair of this.tickers) {
      const data = await this.updatePair(`${pair.crypto.toUpperCase()}${pair.fiat.toUpperCase()}`);
      pair.value = data[0] ?? 0.00;
      pair.change = data[5] ?? 0.00;
    }

    // Dispatch through the websocket
    await Database.set('crypto-tickers', JSON.stringify(this.tickers));

    // Log the status to the console
    Logger.debug(`Cryptoticker updated!`);
    this.isUpdating = false;
  }

  private async updatePair(pair: string): Promise<number[]> {
    let resp;
    try {
      resp = await fetch(`${this.api}/t${pair}`, {
        method: 'GET'
      }).then(async(resp: any) => {
        switch(resp.status) {
          case 503:
            await ServerStatuses.update('BitFinex', 'down');
            return [];
          case 200:
            await ServerStatuses.update('BitFinex', 'up');
            return resp.json();
          default:
            await ServerStatuses.update('BitFinex', 'unknown');
            return [];
        }
      });
    } catch(e) {
      /*
      await Notifications.add({
        service: "Crypto Ticker",
        id: 0,
        severity: "error",
        message: `Could not update ticker "${pair}": ${e.message}`
      });
      */
      Logger.error(`Could not update ticker "${pair}": ${e.message}`);
      return [];
    }

    return resp;
  }
}
