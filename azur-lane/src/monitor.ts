import { Logger, cron } from "../deps.ts";
import { ServerStatuses } from "../../lib/communication/server-status.ts";
import { Database } from "../../lib/communication/database.ts";

interface IServer {
  id: number;
  name: string;
  state: number;
  flag: number;
  sort: number;
}

export class Monitor {
  private api: string = 'http://blhxusgate.yo-star.com/?cmd=load_server?';
  private server: string = 'Washington';
  private isUpdating: boolean = false;

  public constructor() {
    Logger.info(`Starting monitor for "Azur Lane"...`);

    // Set our server from the env
    if(typeof Deno.env.get('AL_SERVER') !== 'undefined') {
      Logger.info(`Changing server to "${Deno.env.get('AL_SERVER')!}"`);
      this.server = Deno.env.get('AL_SERVER')!;
    } else {
      Logger.warning(`Using default server "${this.server}"`);
    }

    // Run updater
    cron('* * * * *', async () => { await this.update(); });
  }

  private async update() {
    // Make sure we're not already updating
    if(this.isUpdating === true) return;

    // Mark as updating
    this.isUpdating = true;
    Logger.debug(`Updating Azur Lane server status...`);

    // Make sure the internet is up
    const hasInternet = await Database.get('internet-status');
    if(!hasInternet) {
      await ServerStatuses.update('Azur Lane', 'unknown');
      this.isUpdating = false;
      return Logger.warning(`No internet, skipping test...`);
    }

    // Get our data from the API
    let resp;
    try {
      resp = await fetch(this.api, {
        method: 'GET'
      });
      resp = await resp.json();
    } catch(e) {
      Logger.error(`Could not update Azur Lane server status: ${e.message}`);
      await ServerStatuses.update('Azur Lane', 'unknown');
      this.isUpdating = false;
      return;
    }

    // Update the status
    const server: IServer = resp.find((server: IServer) => server.name === this.server);
    switch(server.state) {
      case 0:
        await ServerStatuses.update('Azur Lane', 'up');
        Logger.debug(`Azur Lane server test completed! (status: up)`);
        break;
      case 1:
        await ServerStatuses.update('Azur Lane', 'down');
        Logger.debug(`Azur Lane server test completed! (status: down)`);
        break;
      default:
        await ServerStatuses.update('Azur Lane', 'unknown');
        Logger.debug(`Azur Lane server test completed! (status: unknown)`);
        break;
    }

    // Mark as finished
    this.isUpdating = false;
  }
}
