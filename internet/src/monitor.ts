import { Logger, cron } from "../deps.ts";
import { Notifications } from "../../lib/communication/notifications.ts";
import { Database } from "../../lib/communication/database.ts";

export class Monitor {
  private hosts: string[] = ['linux.pizza', 'gitlab.com', 'google.com'];
  private isUpdating: boolean = false;
  private currentStatus: boolean = true;
  private previousStatus: boolean = true;

  public constructor() {
    Logger.info(`Starting monitor for "Internet"...`);

    // Run updater
    this.update();
    cron('* * * * *', async () => { await this.update(); });
  }

  public async update(): Promise<void> {
    // Make sure we don't double-test
    if(this.isUpdating === true) return;

    // Run a test
    Logger.debug(`Attempting to check Internet status...`);
    this.previousStatus = this.currentStatus;
    this.currentStatus = await this.test();
    await Database.set('internet-status',this.currentStatus);

    // Inform through Matrix
    // Only do so if the status has changed
    if(this.currentStatus !== this.previousStatus) {
      if(this.currentStatus) {
        Logger.info(`Internet appears to be up again!`);
        await Notifications.add({
          service: "Internet",
          id: 0,
          severity: "info",
          message: `Internet is online!`
        });
      } else {
        Logger.warning(`Internet appears to be down!`);
        await Notifications.add({
          service: "Internet",
          id: 0,
          severity: "error",
          message: `Internet is offline!`
        });
      }
    }

    // Log the status to the console
    Logger.debug(`Internet test completed! (status: ${this.currentStatus ? 'up' : 'down'})`);

    this.isUpdating = false;
  }

  private test(): Promise<boolean> {
    return new Promise(async (resolve) => {
      // Loop over each host
      // Try to get a response
      // Resolve when a response has been obtained
      // Keep going if one host isn't responding
      for await(const host of this.hosts) {
        let resp;
        try {
          resp = await fetch(`https://${host}`, {
            method: 'HEAD'
          });
        } catch(e) {
          Logger.error(`Could not test internet uptime against "${host}": ${e.message}`);
          continue;
        }

        if(resp.statusText === 'OK') resolve(true);
      }

      // We haven't resolved
      // Internet is likely down
      resolve(false);
    });
  }
}
