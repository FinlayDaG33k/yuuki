import { Logger, cron } from "../deps.ts";
import { Database } from "../../lib/communication/database.ts";
import { Notifications } from "../../lib/communication/notifications.ts";

interface IDiskSpace {
  total: number;
  used: number;
  available: number;
  usePercent: number;
}

export class Monitor {
  private highestUsage: number = 0;

  public constructor() {
    Logger.info(`Starting monitor for "Filesystem"...`);
    this.update();
    cron('* * * * *', async () => { await this.update(); });
  }

  private async update() {
    Logger.debug(`Updating Filesystem usage...`);

    // Get our diskspace
    const data = await this.run();
    const regExp = /(?:[\/[a-zA-Z0-9:]+)\s+([0-9]+)\s+([0-9]+)\s+([0-9]+)\s+([0-9]+)%/gm;
    const matches = regExp.exec(data);
    if(matches === null) return Logger.error('Could not obtain free diskspace!');
    const diskSpace: IDiskSpace = {
      total: Number(matches[1]) ?? 0,
      used: Number(matches[2]) ?? 0,
      available: Number(matches[3]) ?? 0,
      usePercent: Number(matches[4]) ?? 0
    };

    // Update database
    await Database.set('filesystem-usage', JSON.stringify(diskSpace));

    // Lower highest use if below current
    if(diskSpace.usePercent < this.highestUsage) this.highestUsage = diskSpace.usePercent;

    // Only start showing warnings if the diskspace is above 95%
    if(diskSpace.usePercent < 95) return;

    // Prevent duplicate notifications
    if(diskSpace.usePercent === this.highestUsage) return;

    // Add the notification
    await Notifications.add({
      service: "Filesystem",
      id: 0,
      severity: "warning",
      message: `Filesystem has reached a usage of ${diskSpace.usePercent}%`
    });

    // Update the highest usage
    this.highestUsage = diskSpace.usePercent;
  }

  private async run() {
    const cmd = Deno.run({
      cmd: [`df`, `-k`, `${Deno.env.get('DATA_DIR')}`],
      stdout: "piped",
      stderr: "piped"
    });
    const output = new TextDecoder().decode(await cmd.output());
    cmd.close();

    return output;
  }
}
