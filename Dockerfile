FROM denoland/deno:alpine-1.13.2

ARG MODULE="master"

WORKDIR /yuuki/app
ADD ./${MODULE}/entrypoint.ts /yuuki/app
ADD ./${MODULE}/deps.ts /yuuki/app
ADD ./${MODULE}/src /yuuki/app/src
ADD ./lib /yuuki/lib
RUN deno cache entrypoint.ts
RUN deno cache deps.ts

CMD ["deno", "run", "--allow-all", "entrypoint.ts"]
