import { env, Logger, Webserver } from "./deps.ts";
import { Bootstrap } from "../lib/communication/bootstrap.ts";

// Load our env file
const bootstrap = await Bootstrap.getEnv('server-status');
if(!bootstrap) Deno.exit(1);

// Load routes
import "./src/routes.ts";

// Load env
env({
  path: './.env',
  export: true
});

// Start webserver
Logger.info(`Starting webserver on port "${Number(Deno.env.get("WEBSERVER_PORT")) ?? 80}"...`);
const server = new Webserver(Number(Deno.env.get("WEBSERVER_PORT")) ?? 80);
server.start();
Logger.info(`Webserver server started!`);
