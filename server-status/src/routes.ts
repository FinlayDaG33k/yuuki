import { Router } from "../deps.ts";

Router.add({ path: '/list', controller: 'Status', action: 'index', method: 'GET' });
Router.add({ path: '/update/:name', controller: 'Status', action: 'update', method: 'PATCH' });
