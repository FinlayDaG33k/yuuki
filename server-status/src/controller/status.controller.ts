import { Controller, RouteArgs, Time } from "../../deps.ts";
import { Database } from "../../../lib/communication/database.ts";
import { IServerStatus } from "../../../lib/interfaces/serverstatus.interface.ts";

export class StatusController extends Controller {
  private statuses: IServerStatus[] = [];

  public async update(args: RouteArgs) {
    // Make sure a name is set
    if(!args.params.hasOwnProperty('name')) {
      super.set('message', 'ERR_EMPTY_NAME');
      super.type = 'text/plain';
      return;
    }

    // Make sure a body is set
    if(!args.body) {
      super.set('message', 'ERR_EMPTY_STATUS_BODY');
      super.type = 'text/plain';
      return;
    }

    // Make sure the body has one of the allowed statuses
    const allowed = ['up', 'down', 'unknown'];
    if(allowed.indexOf(args.body) === -1) {
      super.set('message', 'ERR_INVALID_STATUS');
      super.type = 'text/plain';
      return;
    }

    // Get our status object from database
    const json = await Database.get(`server-statuses`);
    if(json) { this.statuses = JSON.parse(json) }

    // Replace %20 with actual spaces
    args.params.name = args.params.name.replace('%20', ' ');

    // Check if this service already exists
    // If so, update it
    // Else, add it
    const index = this.statuses.findIndex((item: IServerStatus) => item.server === args.params.name);
    if(index >= 0) {
      this.statuses[index].time = new Time();
      this.statuses[index].status = args.body;
    } else {
      this.statuses.push({
        server: args.params.name,
        status: args.body,
        time: new Time()
      });
    }

    // Send our updated status to the database
    await Database.set(`server-statuses`, JSON.stringify(this.statuses));

    super.set('message', 'OK');
    super.type = 'text/plain';
  }
}
