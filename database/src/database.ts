import { Logger } from "../deps.ts";
import { MasterEvent } from "../../lib/communication.ts";

export class Database {
  private static entries: any = {};

  /**
   * Return an entry from the database
   * Will return null if key doesn't exist
   *
   * @param key
   * @returns any|null
   */
  public static get(key: string): any|null {
    if(!Database.entries.hasOwnProperty(key)) return null;
    return Database.entries[key];
  }

  /**
   * Set an entry in the database.
   * This method will overwrite the old value if existing.
   *
   * @param key The key for the entry
   * @param value The value to set
   * @returns boolean Whether setting was a success
   */
  public static async set(key: string, value: any = null): Promise<boolean> {
    try {
      Database.entries[key] = value;
    } catch(e) {
      Logger.warning(`Could not set database entry "${key}"`);
      Logger.debug(e.message);
      return false;
    }

    // Try to dispatch an event
    await MasterEvent.dispatch('DATABASE_UPDATE', {
      key: key,
      content: value
    });

    // Save our entries
    try {
      await Deno.writeTextFile(`${Deno.env.get("DATA_DIR")}/${key}.json`, JSON.stringify(value));
    } catch(e) {
      Logger.warning(`Could not save key "${key}" to file`);
      Logger.debug(e.message);
    }

    return true;
  }

  /**
   * Delete an entry from the database.
   *
   * @param key The key to delete
   * @returns boolean Whether deletion was a success
   */
  public static async delete(key: string): Promise<boolean> {
    try {
      delete Database.entries[key];
    } catch(e) {
      Logger.warning(`Could not delete database entry "${key}"`);
      Logger.debug(e.message);
      return false;
    }

    // Delete from disk
    await Deno.remove(`${Deno.env.get("DATA_DIR")}/${key}.json`);

    return true;
  }

  /**
   * Dump all entries currently held by the database
   * Please use with caution!
   *
   * @returns any The object containing all database entries
   */
  public static dump(): any { return Database.entries; }

  public static async load() {
    Logger.info('Loading database...');
    for await(const file of Deno.readDir(`${Deno.env.get("DATA_DIR")}`)) {
      const key = file.name.replace('.json', '');
      try {
        Database.entries[key] = JSON.parse(await Deno.readTextFile(`${Deno.env.get("DATA_DIR")}/${file.name}`));
      } catch(e) {
        Logger.error(`Could not read key "${key}"`);
        Logger.debug(e.message);
      }
    }
    Logger.info('Loading database loaded!');
  }
}
