import { Router } from "../deps.ts";

Router.add({path: '/dump', controller: 'Database', action: 'index', method: 'GET'});
Router.add({ path: '/get/:key', controller: 'Database', action: 'display', method: 'GET' });
Router.add({ path: '/set/:key', controller: 'Database', action: 'add', method: 'PUT' });
Router.add({ path: '/remove/:key', controller: 'Database', action: 'remove', method: 'DELETE' });
