import { Controller, RouteArgs } from "../../deps.ts";
import { Database } from "../database.ts";

export class DatabaseController extends Controller {
  public async index(args: RouteArgs) {
    // Check if we are in debug
    // If not, do not dump anything
    if(Deno.env.get('DEBUG') != "true") {
      super.set('data', { message: 'Database dumping only allowed in debug'});
      super.type = 'application/json';
      super.status = 403;
      return;
    }

    super.set('data', { result: Database.dump() });
    super.type = 'application/json';
  }

  public async display(args: RouteArgs) {
    // Make sure a key is set
    if(!args.params.hasOwnProperty('key')) {
      super.set('data', { message: 'ERR_EMPTY_KEY' });
      super.type = 'application/json';
      return;
    }

    const result = Database.get(args.params.key);
    super.set('data', { result: result });
    super.type = 'application/json';
  }

  public async add(args: RouteArgs) {
    // Make sure a key is set
    if(!args.params.hasOwnProperty('key')) {
      super.set('message', 'ERR_EMPTY_KEY');
      super.type = 'text/plain';
      return;
    }

    // Make sure a body is set
    if(!args.body) {
      super.set('message', 'ERR_EMPTY_BODY');
      super.type = 'text/plain';
      return;
    }

    if(!await Database.set(args.params.key, args.body)) {
      super.set('message', 'ERR_ADD');
      super.type = 'text/plain';
      return;
    }

    super.set('message', 'OK');
    super.type = 'text/plain';
  }

  public async remove(args: RouteArgs) {
    // Make sure a key is set
    if(!args.params.hasOwnProperty('key')) {
      super.set('message', 'ERR_EMPTY_KEY');
      super.type = 'text/plain';
      return;
    }

    // Remove the key from database
    if(!await Database.delete(args.params.key)) {
      super.set('message', 'ERR_DELETE');
      super.type = 'text/plain';
      super.status = 500;
      return;
    }

    super.set('message', 'OK');
    super.type = 'text/plain';
  }
}
