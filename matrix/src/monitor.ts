import { Logger, cron } from "../deps.ts";
import { Notifications } from "../../lib/communication/notifications.ts";
import { Database } from "../../lib/communication/database.ts";

export class Monitor {
  private host: string = 'https://matrix.finlaydag33k.nl';
  private isUpdating: boolean = false;
  private status: boolean = true;
  private previousStatus: boolean = true;

  public constructor() {
    Logger.info(`Starting monitor for "Matrix Federation"...`);

    // Set our server from the env
    if(typeof Deno.env.get('MATRIX_HOST') !== 'undefined') {
      Logger.info(`Changing server to "${Deno.env.get('MATRIX_HOST')!}"`);
      this.host = Deno.env.get('MATRIX_HOST')!;
    } else {
      Logger.warning(`Using default server "${this.host}"`);
    }

    // Run updater
    cron('* * * * *', async () => { await this.update(); });
  }

  public async update() {
    // Make sure we're not already updating
    if(this.isUpdating === true) return;

    // Mark as updating
    this.isUpdating = true;
    Logger.debug(`Updating Matrix federation status...`);

    // Make sure the internet is up
    const hasInternet = await Database.get('internet-status');
    if(!hasInternet) {
      this.isUpdating = false;
      return Logger.warning(`No internet, skipping test...`);
    }

    // Update the status
    await this.run();
    Logger.debug(`Matrix federation test completed! (status: ${this.status ? 'up' : 'down'})`);

    // Add notification if need be
    if(this.status !== this.previousStatus) {
      if(this.status) {
        Logger.info(`Matrix server is federating properly again!`);
        await Notifications.add({
          service: "Matrix Federation",
          id: 0,
          severity: "error",
          message: `Federation for "${this.host}" has been restored!`
        });
      } else {
        Logger.warning(`Matrix server isn't federating properly!`);
        await Notifications.add({
          service: "Matrix Federation",
          id: 0,
          severity: "error",
          message: `Federation for "${this.host}" has failed!`
        });
      }
    }
  }

  public async run(): Promise<void> {
    // Try to get a response from the federation tester
    let resp;
    try {
      resp = await fetch(`https://federationtester.matrix.org/api/report?server_name=${this.host}`)
        .then(response => response.json());
    } catch (e) {
      await Notifications.add({
        service: "Matrix Federation",
        id: 0,
        severity: "error",
        message: `Could not get federation test: ${e.message}`
      });
      Logger.error(`Could not get federation test: ${e.message}`);
      return;
    }

    // Update our status
    this.previousStatus = this.status;
    this.status = resp.FederationOK;
  }
}
