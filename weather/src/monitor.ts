import { Logger } from "../deps.ts";
import { cron } from "../deps.ts";
import { Database } from "../../lib/communication/database.ts";

export class Monitor {
  private isUpdating: boolean = false;
  private owm: string;

  constructor() {
    Logger.info(`Starting monitor for "Weather"...`);
    this.owm = `${Deno.env.get('OWM_HOST')}?units=metric&lat=${Deno.env.get('OWM_LAT')}&lon=${Deno.env.get('OWM_LON')}&appid=${Deno.env.get('OWM_KEY')}`;
    cron('*/10 * * * *', async () => { await this.update(); });
  }

  public async update() {
    // Make sure we're not already updating
    if(this.isUpdating === true) return;

    // Mark as updating
    this.isUpdating = true;
    Logger.debug(`Updating Weather...`);

    // Make sure our internet is up
    const hasInternet = await Database.get('internet-status');
    if(!hasInternet) {
      this.isUpdating = false;
      return Logger.warning(`No internet, skipping update...`);
    }

    // Download data from API
    const resp = await fetch(this.owm).then(response => response.json());

    // Store the weather data in database
    await Database.set('weather', JSON.stringify(resp));

    // Mark as finished
    this.isUpdating = false;
  }
}
