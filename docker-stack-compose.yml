version: '3.7'
services:
  master:
    image: 'registry.gitlab.com/finlaydag33k/yuuki/master:${COMMIT}'
    networks:
      - default
      - traefik_traefik-net
      - bridge
    dns:
      - 10.0.0.1
    environment:
      - BOOTSTRAP_TOKEN=${BOOTSTRAP_TOKEN}
    secrets:
      - source: MASTER_ENV
        target: /yuuki/app/.env
      - source: BOOTSTRAP_CONFIG
        target: /yuuki/app/bootstrap.json
    deploy:
      labels:
        - "traefik.enable=true"
        - "traefik.docker.network=traefik_traefik-net"
        - "traefik.http.routers.FinlayDaG33k-Yuuki-http.service=FinlayDaG33k-Yuuki-http"
        - "traefik.http.routers.FinlayDaG33k-Yuuki-https.service=FinlayDaG33k-Yuuki-https"
        - "traefik.http.routers.FinlayDaG33k-Yuuki-wss.service=FinlayDaG33k-Yuuki-wss"
        - "traefik.http.services.FinlayDaG33k-Yuuki-http.loadbalancer.server.port=80"
        - "traefik.http.services.FinlayDaG33k-Yuuki-https.loadbalancer.server.port=80"
        - "traefik.http.services.FinlayDaG33k-Yuuki-wss.loadbalancer.server.port=81"
        - "traefik.http.middlewares.FinlayDaG33k-Yuuki-http.redirectscheme.scheme=https"
        - "traefik.http.middlewares.FinlayDaG33k-Yuuki-http.redirectscheme.permanent=true"
        - "traefik.http.routers.FinlayDaG33k-Yuuki-http.entrypoints=http"
        - "traefik.http.routers.FinlayDaG33k-Yuuki-http.rule=Host(`yuuki.finlaydag33k.nl`)"
        - "traefik.http.routers.FinlayDaG33k-Yuuki-https.entrypoints=https"
        - "traefik.http.routers.FinlayDaG33k-Yuuki-https.rule=Host(`yuuki.finlaydag33k.nl`)"
        - "traefik.http.routers.FinlayDaG33k-Yuuki-https.tls.certresolver=finlaydag33k_nl"
        - "traefik.http.routers.FinlayDaG33k-Yuuki-https.tls=true"
        - "traefik.http.routers.FinlayDaG33k-Yuuki-wss.entrypoints=https"
        - "traefik.http.routers.FinlayDaG33k-Yuuki-wss.rule=(Host(`yuuki.finlaydag33k.nl`) && Headers(`Upgrade`, `websocket`))"
        - "traefik.http.routers.FinlayDaG33k-Yuuki-wss.tls.certresolver=finlaydag33k_nl"
        - "traefik.http.routers.FinlayDaG33k-Yuuki-wss.tls=true"
  database:
    image: 'registry.gitlab.com/finlaydag33k/yuuki/database:${COMMIT}'
    networks:
      - default
    environment:
      - MASTER_HOST=${MASTER_HOST}
      - BOOTSTRAP_TOKEN=${BOOTSTRAP_TOKEN}
    volumes:
      - type: volume
        source: db-data
        target: /yuuki/data
  notifications:
    image: 'registry.gitlab.com/finlaydag33k/yuuki/notifications:${COMMIT}'
    networks:
      - default
    environment:
      - MASTER_HOST=${MASTER_HOST}
      - BOOTSTRAP_TOKEN=${BOOTSTRAP_TOKEN}
    volumes:
      - type: volume
        source: notification-data
        target: /yuuki/data
  server-status:
    image: 'registry.gitlab.com/finlaydag33k/yuuki/server-status:${COMMIT}'
    networks:
      - default
    environment:
      - MASTER_HOST=${MASTER_HOST}
      - BOOTSTRAP_TOKEN=${BOOTSTRAP_TOKEN}
  crypto:
    image: 'registry.gitlab.com/finlaydag33k/yuuki/crypto:${COMMIT}'
    networks:
      - default
      - bridge
    environment:
      - MASTER_HOST=${MASTER_HOST}
      - BOOTSTRAP_TOKEN=${BOOTSTRAP_TOKEN}
  filesystem:
    image: 'registry.gitlab.com/finlaydag33k/yuuki/filesystem:${COMMIT}'
    networks:
      - default
    environment:
      - MASTER_HOST=${MASTER_HOST}
      - BOOTSTRAP_TOKEN=${BOOTSTRAP_TOKEN}
    volumes:
      - type: volume
        source: filesystem-data
        target: /yuuki/data
  internet:
    image: 'registry.gitlab.com/finlaydag33k/yuuki/internet:${COMMIT}'
    networks:
      - default
      - bridge
    environment:
      - MASTER_HOST=${MASTER_HOST}
      - BOOTSTRAP_TOKEN=${BOOTSTRAP_TOKEN}
  matrix:
    image: 'registry.gitlab.com/finlaydag33k/yuuki/matrix:${COMMIT}'
    networks:
      - default
      - bridge
    environment:
      - MASTER_HOST=${MASTER_HOST}
      - BOOTSTRAP_TOKEN=${BOOTSTRAP_TOKEN}
  ups:
    image: 'registry.gitlab.com/finlaydag33k/yuuki/ups:${COMMIT}'
    networks:
      - default
      - bridge
    environment:
      - MASTER_HOST=${MASTER_HOST}
      - BOOTSTRAP_TOKEN=${BOOTSTRAP_TOKEN}
  website:
    image: 'registry.gitlab.com/finlaydag33k/yuuki/website:${COMMIT}'
    networks:
      - default
      - bridge
    environment:
      - MASTER_HOST=${MASTER_HOST}
      - BOOTSTRAP_TOKEN=${BOOTSTRAP_TOKEN}
  azur-lane:
    image: 'registry.gitlab.com/finlaydag33k/yuuki/azur-lane:${COMMIT}'
    networks:
      - default
      - bridge
    environment:
      - MASTER_HOST=${MASTER_HOST}
      - BOOTSTRAP_TOKEN=${BOOTSTRAP_TOKEN}
  schedule:
    image: 'registry.gitlab.com/finlaydag33k/yuuki/schedule:${COMMIT}'
    networks:
      - default
      - bridge
    environment:
      - MASTER_HOST=${MASTER_HOST}
      - BOOTSTRAP_TOKEN=${BOOTSTRAP_TOKEN}
  weather:
    image: 'registry.gitlab.com/finlaydag33k/yuuki/weather:${COMMIT}'
    networks:
      - default
      - bridge
    environment:
      - MASTER_HOST=${MASTER_HOST}
      - BOOTSTRAP_TOKEN=${BOOTSTRAP_TOKEN}
networks:
  default:
    internal: true
    driver: overlay
  traefik_traefik-net:
    external: true
  bridge:
    external: true  
secrets:
  MASTER_ENV:
    name: FinlayDaG33k-yuuki-${ENVIRONMENT}_MASTER_ENV-${COMMIT}
    external: true
  BOOTSTRAP_CONFIG:
    name: FinlayDaG33k-yuuki-${ENVIRONMENT}_BOOTSTRAP_CONFIG-${COMMIT}
    external: true
volumes:
  db-data:
    driver: local
  notification-data:
    driver: local
  filesystem-data:
    driver: local
