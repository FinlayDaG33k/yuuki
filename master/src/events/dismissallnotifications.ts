import { Logger } from "../../deps.ts";
import { Notifications } from "../../../lib/communication.ts";

export class DismissAllNotificationEvent {
  public async execute(data: any) {
    // Remove the notification
    Logger.info(`Dismissing notification #${data.id}`);
    await Notifications.removeAll();
    return;
  }
}
