import { Database } from "../../../lib/communication.ts";

export class UpdateAvailabilityEvent {
  public async execute(data: any) {
    await Database.set('availability-status', data.status);
  }
}
