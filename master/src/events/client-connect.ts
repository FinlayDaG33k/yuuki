import { Database, Notifications } from "../../../lib/communication.ts";

export class ClientConnectEvent {
  public async execute(data: any) {
    // Make sure the client is still alive
    if(!data.client) return;
    if(data.client.isClosed) return;

    // Push notifications
    data.client.send(JSON.stringify({
      event: 'NOTIFICATION_UPDATE',
      data: await Notifications.list()
    }));

    // Push Cryptoticker
    data.client.send(JSON.stringify({
      event: 'DATABASE_UPDATE',
      data: {
        key: 'crypto-tickers',
        content: await Database.get('crypto-tickers')
      }
    }));

    // Push serverstatus
    data.client.send(JSON.stringify({
      event: 'DATABASE_UPDATE',
      data: {
        key: 'server-statuses',
        content: await Database.get('server-statuses')
      }
    }));

    // Push UPS status
    data.client.send(JSON.stringify({
      event: 'DATABASE_UPDATE',
      data: {
        key: 'ups-state',
        content: await Database.get('ups-state')
      }
    }));

    // Push Schedule
    data.client.send(JSON.stringify({
      event: 'DATABASE_UPDATE',
      data: {
        key: 'ical-schedule',
        content: await Database.get('ical-schedule')
      }
    }));

    // Push Weather
    data.client.send(JSON.stringify({
      event: 'DATABASE_UPDATE',
      data: {
        key: 'weather',
        content: await Database.get('weather')
      }
    }));

    // Push Availability
    data.client.send(JSON.stringify({
      event: 'DATABASE_UPDATE',
      data: {
        key: 'availability-status',
        content: await Database.get('availability-status')
      }
    }));
  }
}
