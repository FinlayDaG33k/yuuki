import { Events } from "../deps.ts";

await Events.add({name: 'ClientConnect', handler: 'client-connect'});
await Events.add({name: 'DismissNotification', handler: 'dismissnotifications'});
await Events.add({name: 'DismissAllNotification', handler: 'dismissallnotifications'});
await Events.add({name: 'UpdateAvailability', handler: 'update-availability'});
