import { Router } from "../deps.ts";

Router.add({ path: '/', controller: 'Home', action: 'index', method: 'GET' });
Router.add({ path: '/availability', controller: 'Availability', action: 'index', method: 'GET'});
Router.add({ path: '/event', controller: 'Event', action: 'index', method: 'POST'});
Router.add({ path: '/bootstrap/:module', controller: 'Bootstrap', action: 'index', method: 'GET'});
