import { Controller, RouteArgs } from "../../deps.ts";

export class HomeController extends Controller {
  public async index(args: RouteArgs) {
    super.type = 'text/plain';
    super.set('message', 'Yuuki is up and running! :D');
  }
}
