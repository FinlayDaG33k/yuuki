import { Controller, RouteArgs } from "../../deps.ts";
import {Database} from "../../../lib/communication.ts";

export class AvailabilityController extends Controller {
  public async index(args: RouteArgs) {
    let status = await Database.get('availability-status');
    if(!status) status = 'unknown';

    super.type = 'text/plain';
    super.set('message', status);
  }
}
