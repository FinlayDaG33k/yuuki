import { Controller, RouteArgs, Websocket } from "../../deps.ts";

// Allow websocket in window object
declare global {
  interface Window {
    websocket: Websocket;
  }
}

interface IEvent {
  event: string;
  data: any;
}

export class EventController extends Controller {
  public async index(args: RouteArgs) {
    // Check if we have the right Bearer token
    if(args.auth !== `Bearer ${Deno.env.get('AUTH_SECRET')}`) {
      super.type = 'text/plain';
      super.set('message', 'ERR_INVALID_BEARER_TOKEN');
      super.status = 403;
      return;
    }

    // Make sure we have a body
    if(!args.body) {
      super.type = 'text/plain';
      super.set('message', 'ERR_EMPTY_BODY');
      return;
    }

    // Decode the body
    const data: IEvent = JSON.parse(args.body);
    window.websocket.broadcast(data.event, data.data);

    super.type = 'text/plain';
    super.set('message', 'OK');
  }
}
