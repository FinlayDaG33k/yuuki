import { Logger, Controller, RouteArgs } from "../../deps.ts";

export class BootstrapController extends Controller {
  public async index(args: RouteArgs) {
    // Check if we have the right Bearer token
    if(args.auth !== `Bearer ${Deno.env.get('BOOTSTRAP_TOKEN')}`) {
      Logger.debug(`Invalid bootstrap token provided: "${Deno.env.get('BOOTSTRAP_TOKEN')}"`);
      super.type = 'text/plain';
      super.set('message', 'ERR_INVALID_BEARER_TOKEN');
      super.status = 403;
      return;
    }

    // Make sure a module is specified
    if(!args.params.module) {
      super.type = 'text/plain';
      super.set('message', 'ERR_UNSPECIFIED_MODULE');
      super.status = 400;
      return;
    }

    // Read our config
    const bootstrap = await this.readConfig();

    // Check if our config has the requested module
    if(!bootstrap.hasOwnProperty(args.params.module)) {
      super.type = 'text/plain';
      super.set('message', 'ERR_INVALID_MODULE');
      super.status = 404;
      return;
    }

    // Build our env
    let data: string = '';
    if(bootstrap[args.params.module]['require-database'] == true) data += `DATABASE_HOST="${Deno.env.get("DATABASE_HOST")}"\r\n`;
    if(bootstrap[args.params.module]['require-notification'] == true) data += `NOTIFICATION_HOST="${Deno.env.get("NOTIFICATION_HOST")}"\r\n`;
    if(bootstrap[args.params.module]['require-server-status'] == true) data += `STATUS_HOST="${Deno.env.get("STATUS_HOST")}"\r\n`;
    data += `AUTH_SECRET="${Deno.env.get("AUTH_SECRET")}"\r\n`;
    for(const [key, value] of Object.entries(bootstrap[args.params.module].vars)) {
      data += `${key.toUpperCase()}="${value}"\r\n`;
    }

    super.type = 'text/plain';
    super.set('message', data);
  }

  private async readConfig() {
    const data = await Deno.readTextFile(`${Deno.cwd()}/bootstrap.json`);
    return JSON.parse(data);
  }
}
