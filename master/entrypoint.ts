import { env, Logger, Webserver, Websocket } from "./deps.ts";

// Allow websocket in window object
declare global {
  interface Window {
    websocket: Websocket;
  }
}

// Load our routes and events
import "./src/routes.ts";
import "./src/events.ts";

// Load env
env({
  path: './.env',
  export: true
});

// Start webserver
Logger.info(`Starting webserver on port "${Number(Deno.env.get("WEBSERVER_PORT")) ?? 80}"...`);
const webserver = new Webserver(Number(Deno.env.get("WEBSERVER_PORT")) ?? 80);
webserver.start();
Logger.info(`Webserver server started!`);

// Start websocket server
Logger.info(`Starting websocket server on port "${Number(Deno.env.get("WEBSOCKET_PORT")) ?? 81}"`);
window.websocket = new Websocket(Number(Deno.env.get("WEBSOCKET_PORT")) ?? 81, true);
await window.websocket.start();
Logger.info(`Websocket server started!`);
